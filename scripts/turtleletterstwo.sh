# !/ usr / bin / bash
rosservice call /reset
rosservice call /turtle1/set_pen 255 255 255 2 true
rosservice call /turtle1/teleport_absolute 2.0 4.0 0.0
rosservice call /turtle1/set_pen 255 0 0 2 false
rosservice call /spawn 5.5 4.0 0.0 'turtle2'
rosservice call /turtle2/set_pen 0 255 0 2 false
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , -4.0 , 0.0]' '[0.0 , 0.0 , 4]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , -4.0 , 0.0]' '[0.0 , 0.0 , -4.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[5.0 , -5.0 , 0.0]' '[0.0 , 0.0 , 6.5]'

